# EUPT XML

(Vorerst beinhaltet diese Readme die Anfänge einer Dokumentation des Schemas)

## Schemadokumentation

### Metadaten

#### Header

Mittels `header` werden die Metadaten erfasst.

#### Titel der Tontafel

Mittels `title` wird der Titel der Tontafel erfasst.

##### Beispiel

```
<title>KTU 1.14</title>
```

#### Genre

Das Genre des Textes wird mittels `textClass` und `classCode` erfasst.

Mögliche Werte sind folgende:

* epic (Epos)
* historiola (Historiola)
* hymn (Hymnus)
* incantation (Beschwörung)
* prayer (Gebet)
* ritual (Ritual)

##### Beispiel

```
<textClass>
    <classCode>epic</classCode>
</textClass>
```

#### Editor*innen

Die Editor*innen werden in `editionStmt` in je einem `editor` erfasst. Das `editor` wird mit `@xml:id` spezifiziert. Der Name wird mittels `name/forename` und `name/surname` erfasst. Das `name` kann mit `@gnd` und `@orcid` spezifiziert werden.

##### Beispiel

```
<editionStmt>
    <editor xml:id="RMu">
        <name gnd="https://d-nb.info/gnd/128892935" orcid="https://orcid.org/0000-0002-7433-2273">
           <forename>Reinhard</forename>
           <surname>Müller</surname>
        </name>
    </editor>
</editionStmt>
```

#### Titel von EUPT

Der Titel von EUPT wird mittels `seriesStmt/title` erfasst.

##### Beispiel

```
<seriesStmt>
    <title>Edition des ugaritischen poetischen Textkorpus (EUPT)</title>
</seriesStmt>
```

#### Abstract

Ein Abstract wird mittels `sourceDesc/abstract/p` erfasst.

##### Beispiel

```
<abstract>
    <p>Erste Tafel des <ref target="https://eupt.uni-goettingen.de/Einfuehrung/Textuebersicht.html"
          >Kirtu-Epos</ref>.</p>
</abstract>
```

#### Identifikation der Tontafel

Die Tontafel wird mittels `sourceDesc/msDesc/msIdentifier` erfasst:

* Ort: `settlement/@geoNames`
* Repository: `repository`
* Grabungsnummer: `idno@type="Grabungsnummer"`
* Museumsnummer: `idno@type="Museumsnummer`
* Publikationsnummer: `idno@type="Publikationsnummer`

##### Beispiel

```
<msIdentifier>
   <repository>(Musée National d'Alep)</repository>
   <idno type="Grabungsnummer">RS 2.[003] + 3.324 + 3.344 + 3.414</idno>
   <idno type="Museumsnummer">M 8218 = A 2750 (AO 17.190)</idno>
   <idno type="Publikationsnummer">KTU<hi rend="super">1-3</hi> 1.14</idno>
   <idno type="Publikationsnummer">CTA 14</idno>
   <idno type="Publikationsnummer">UDB 14</idno>
</msIdentifier>
```

#### Beschreibung der Tontafel

Die Tontafel wird mittels `sourceDesc/msDesc/physDesc/objectDesc` beschrieben:

* Form: `@form`
* Material: `supportDesc/support`
* Höhe: `supportDesc/extent/dimensions/height`
* Breite: `supportDesc/extent/dimensions/width`
* Tiefe: `supportDesc/extent/dimensions/depth`
* Weitere Beschreibung der Ausmaße: `supportDesc/extent`
* Anzahl an Spalten: `layoutDesc/layout/@columns`
* Beschreibung des Layouts: `layoutDesc/layout`

##### Beispiel

```
<objectDesc form="Tafel">
  <supportDesc>
    <support>Ton</support>
    <extent>
      <dimensions>
        <height>220</height>
        <width>170</width>
        <depth>30</depth>
      </dimensions>
    </extent>
  </supportDesc>
  <layoutDesc>
    <layout columns="6">drei Kolumnen pro Tafelseite.</layout>
  </layoutDesc>
</objectDesc>
```

#### Angabe des Schreibers

Der bzw. die Schreiber wird mittels `sourceDesc/msDesc/physDesc/handDesc/p` angegeben.

##### Beispiel

```
<handDesc>
  <p>Schreiber: ˀIlimilku</p>
</handDesc>
```

#### DecoDesc

Die DecoDesc mittels `sourceDesc/msDesc/physDesc/decoDesc/p` angegeben.

##### Beispiel

noch kein Beispiel verfügbar

#### Ursprung der Tontafel

Der Ursprung der Tontafel wird mittels `sourceDesc/history/origin/p` angegeben.

##### Beispiel

```
<origin>
  <p>Ugarit / Ras Shamra; s. <reftarget="https://pi.lib.uchicago.edu/1001/org/ochre/36caa276-991b-4073-a375-d8b7ba2ab4b7">Ras Shamra Tablet Inventory (RS 2.[003]+ / Kirta 1)</ref>.</p>
</origin>
```

#### Provenienz

Die Provenienz der Tontafel wird mittels `sourceDesc/history/provenance/p` angegeben.

##### Beispiel

noch kein Beispiel verfügbar

#### Zugehörige Ressourcen

Zur Tontafel zugehörige Ressourcen werden mittels `sourceDesc/additional` erfasst:

* Überschrift Bilddateien: `surrogates/head`
* Bilddateien: `surrogates/graphic`
* Link zur Bilddatei: `surrogates/graphic/@url`
* Überschrift Literatur: `listBibl/head`
* Literatur: `listBibl/bibl`
* Link zum jeweiligen Zotero-Eintrag: `listBibl/bibl/@zotero` (mit Prefix "eupt:" und ID)

Es können mehrere Literaturlisten angegeben werden. Diese können mit `listBibl/@type` typisiert werden.

##### Beispiel

```
<additional>
  <surrogates>
    <graphic url="https://digitallibrary.usc.edu/asset-management/2A3BF1S29YBMR?WS=SearchResults">KTU 1.14 Vs. (sw) (WSRP UC15302942)</graphic>
  </surrogates>
  <listBibl type="fotos">
    <bibl zotero="eupt:EWRXVQWT">Virolleaud, 1936: Pl. III-IV</bibl>
  </listBibl>
  <listBibl type="copies">
    <bibl zotero="eupt:EWRXVQWT">Virolleaud, 1936: Pl. I-II</bibl>
  </listBibl>
  <listBibl type="studies">
    <bibl zotero="eupt:D2G2WWRT">Engnell, 1943: 152 (i 31-43; TR, ÜS, KO)</bibl>
  </listBibl>
</additional>
```

#### Abgrenzung einer Spalte

In den Include-Dokumenten wird die Abgrenzung der Spalte mittels `encodingDesc/tagsDecl/rendition` angegeben. Dabei wird eine ID-Angegeben (`rendition/@xml:id`) und optional auf ein Schema verwiesen (`rendition/@scheme`). Zudem wird in textlicher Form die Abgrenzung beschrieben.

##### Beispiel

```
<encodingDesc>
  <tagsDecl>
    <rendition xml:id="doubleLine">Doppelte, horizontale Trennlinie</rendition>
  </tagsDecl>
</encodingDesc>
```

#### Sprache und Schrift

Die Sprache und Schrift werden mittels `profileDesc/langUsage/language` erfasst. `langUsage` wird dabei mit `@type` spezifiert. Mögliche Werte sind `language` (langUsage beschreibt die Sprache) und `writingSystem` (langUsage beschreibt die verwendete Schrift). Das `language` kann mit `@ident` auf einen Sprachcode verweisen.

##### Beispiel

```
<profileDesc>
    <langUsage type="language">
        <language ident="uga">ugaritisch</language>
    </langUsage>
    <langUsage type="writingSystem">
        <language>Keilalphabetisch</language>
    </langUsage>
</profileDesc>
```

#### Externe Ressourcen

Datenbanken, auf die verwiesen wird (und deren Daten in der Serialisierung genutzt werden), werden mittels `externalResources` erfasst:

* Wörterbuch: `dictionary/@href`
* Lemma des Wörterbuchs: `dictionary/@prefix`
* Vokabular: `vocabulary/@href`
* Lemma des Vokabulars: `vocabulary/@prefix`
* Zotero-Bibliothek: `zotero/@xml:base`
* Lemma der Zotero-Bibliothek: `zotero/@prefix`

##### Beispiel

```
<externalResources>
  <dictionary prefix="lemma" href="Lexikon.41tvx.0.xml"/>
  <vocabulary prefix="poet" href="Poetologisches%20Glossar.41tvz.0.xml"/>
  <vocabulary prefix="motif" href="Motiv-Glossar.41tw0.0.xml"/>
  <zotero prefix="eupt" xml:base="https://www.zotero.org/groups/5113405/eupt/collections/WDM6KU22/items/"/>
</externalResources>
```

#### Lizenz
Lizenzangaben werden mittels `tei:availability/tei:licence` erfasst. In `tei:licence` wird als textlicher Inhalt der offizielle Name der Lizenz angegeben. Mit dem Attribut `@target` wird der URI zur ausführlichen Definition der Lizenzbestimmungen angegeben.

##### Beispiel:
```
<tei:availability>
    <tei:licence target="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA-4.0</tei:licence>
</tei:availability>
```

Dies entspricht den [Empfehlungen der KIM-Gruppe Lizenzen](https://wiki.dnb.de/pages/viewpage.action?pageId=217533672).

##### HTML-Serialisierung
Die Lizenzangaben werden im Metadatenpanel angegeben.

#### Versionierung

Die Versionierung wird mittels `revisionDesc/change` erfasst.

Das `change` wird mit folgenden Attributen spezifiziert:

* `@when`: Datum im Format "YYYY-MM-DD"
* `@status`: "draft" (Dokument ist ein Entwurf) oder "final" (Das Dokument ist fertiggestellt)
* `@editor`: Verweist auf die `@xml:id` eines `editor` (wird als "bearbeitet von ..." in der Zitationsempfehlung serialisiert)
* `@coEditor`: Verweist auf die `@xml:id` eines `editor` (wird als "unter Mitarbeit von ..." in der Zitationsempfehlung serialisiert)

##### Beispiel

```
<revisionDesc>
 <change when="2024-08-08" status="draft" editor="#CSt" coEditor="#NKr #CMe #RMu #MPo #FSm">Version Draft 2.1</change>
 <change when="2024-08-06" status="draft" editor="#CSt" coEditor="#NKr #CMe #RMu #MPo #FSm">Version Draft 2.0</change>
 <change when="2024-05-07" status="draft" editor="#CSt" coEditor="#NKr #CMe #RMu #MPo #FSm">Version Draft 1.0</change>
</revisionDesc>
```

#### Status des Dokuments

Der Status des Dokuments wird in `docStatus` mit dem Attribut `@status` erfasst. Als Werte sind `draft` und `published` erlaubt.

##### Beispiel
```
<docStatus status="published"/>
```

##### HTML-Serialisierung

Nur Dokumente mit `@status="published` werden auf der öffentlichen Instanz der Website angezeigt.

### Daten

#### Hervorhebungen
Hervorhebungen werden mittels `hi` erfasst. Die Art der Hervorhebung wird mittels `@rend` definiert. Mögliche Werte für `@rend` sind:

- `bold`: Fett
- `italics`: Kursiv (Default)
- `small_caps`: Kapitälchen
- `sub`: Tiefgestellt
- `super`: Hochgestellt
- `under`: Unterstrichen

##### Beispiel
```
In der Abfolge der Erzählsequenzen <hi rend="bold">Angebot</hi> - <hi rend="under">Ablehnung des Angebots</hi> - <hi>Äußerung des eigentlichen Wunsches</hi> erhält Kirtus tatsächliches Anliegen nämlich besonderes Gewicht:
```

##### HTML-Serialisierung
Darstellung des Textes entsprechend des Werts von `@rend`.

#### Kommentare
Kommentare werden mittels `note` erfasst. Mittels `@target` können Kommentare auf verschiedene Elemente verweisen (bspw. `seg`, `g`, `part`, `phr`, `w`).

Kommentare können einen oder mehrere Typen zugewiesen werden. Hierfür wird das Attribut `@type` genutzt. Erlaubte Werte sind:

  - "con": Inhalt
  - "epi": Epigrafie
  - "gr": Grammatik
  - "lx": Lexik
  - "poet": Poetologie

Kommentare können mehrere Absätze enthalten. Dann muss der Inhalt des `note` ausschließlich aus [`p`](#absätze) bestehen (das `note` darf also keinen textlichen Inhalt haben).

Alternativ kann das `note` in beliebiger Reihenfolge eine beliebige Anzahl an [`ana`](#ana), [`hi`](#hervorhebungen), [`quote`](#zitate), [`ref`](#verweise), [`textBlock`](#ugaritische-textblöcke), [`bibl`](#literaturverweise) und Text enthalten.

#### Absätze

#### Ugaritische Textblöcke
Ugaritischer Text außerhalb der [Transliteration](#transliteration) und [Vokalisation](#vokalisation-und-übersetzung) wird mittels `textBlock` erfasst.

`textBlock` darf eine beliebige Anzahl Elemente aus einer der folgenden Elementgruppen enthalten:

- [`column`](#kolumns)
- [`line`](#zeilen) und [`tei:damage`](#damage)
- [`part`](#part), [`seg`](#seg), [`g`](#g), [`lb`](#linebeginning) sowie die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription)
- [`pc`](#pc), [`phr`](#phr), [`w`](#w), [`lb`](#linebeginning) sowie die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription)

##### Beispiel
```
<textBlock><tei:supplied>D</tei:supplied> <segg>ṮDṮ</seg></textBlock>
```

##### HTML-Serialisierung
Der ugaritische Text soll genauso dargestellt werden wie in der [Transliteration](#transliteration) und in der [Vokalisation](#vokalisation-und-übersetzung) .

#### Zitate
Zitate werden mittels `quote` erfasst.

Mittels `@rend` wird angegeben, in welche Anführungszeichen das Zitat eingefügt wird. Mögliche Werte sind:

  - "de": Deutsche Anführungszeichen („ “ bzw. ‚ ‘)
  - "en": Englische Anführungszeichen (“ ” bzw. ‘ ’)
  - "fr": Französische Anführungszeichen (« » bzw. ‹ ›)

`quote` darf in beliebiger Reihenfolge eine beliebige Anzahl an [`ana`](#ana), [`hi`](#hervorhebungen), [`quote`](#zitate), [`ref`](#verweise), [`bibl`](#literaturverweise), [`pc`](#pc), [`phr`](#phr), [`w`](#w), [`lb`](#lb), [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

##### HTML-Serialisierung
In der HTML-Serialisierung werden Anführungszeichen entsprechend dem Wert in `@rend` vor und nach dem Zitat eingefügt. Wenn das `quote` einen `ancestor::quote` hat, werden einfache Anführungszeichen eingefügt.
 
#### Literaturverweise

#### Verweise
Verweise (Links) werden mittels `ref` erfasst. Dabei wird `@target` genutzt, um das Ziel des Links zu erfassen.

`ref` darf [`hi`](#hervorhebungen), [`textBlock`](#ugaritische-textblöcke) und Text enthalten.

#### Ana

#### Linebeginning

#### Steuerzeichen

Steuerzeichen im ugaritischen Text (Worttrenner, Markierungen für unindentifizierbare Zeichen) werden mittels `pc` erfasst.

Zudem wird `pc` mittels `@type` spezifiziert:
- "non_identifiable_sign_multi" für eine unbestimmte Anzahl unidentifizierbarer Zeichen
- "non_identifiable_sign_single" für ein einzelnes unidentifizierbares Zeichen
- "word_boundary_separator" für Worttrenner

`pc` kann eine `@xml:id` zugewiesen werden. Mittels `@cert` kann angegeben werden, ob man sich sicher ist, dass an dieser Stelle tatsächlich das erfasste Zeichen steht. Wenn ja, wird das `pc` mit `@cert="high"` spezifiziert, wenn nicht dann mit `@cert="low"`. Der Default-Wert ist "high".

##### HTML-Serialisierung
In der HTML-Serialisierung wird `pc` durch einen Punkt mit vorangehendem und folgendem schmalem Leerzeichen ersetzt.

#### metamark

### Transliteration

#### Kolumns

Die Textspalten einer Tontafel werden mittels `column` erfasst.

`column` wird mittels `@n` eine Nummer zugewiesen. Es ist möglich, `column` eine `@xml:id` und/oder `xml:base` zuzuweisen.

`column` darf [`line`](#zeilen), [`note`](#kommentare), [`tei:damage`](#teidamage) sowie [`metamark`](#metamark) enthalten.

#### Zeilen

#### Part

#### Seg

#### G
Ein Zeichen oder eine Glyphe wird mittels `g` erfasst.

`g` kann eine `@xml:id` sowie ein `@ana` zugewiesen werden. Mittels `@cert` kann angegeben werden, ob man sich sicher ist, dass an dieser Stelle tatsächlich das erfasste Zeichen steht. Wenn ja, wird das `g` mit `@cert="high"` spezifiziert, wenn nicht dann mit `@cert="low"`. Der Default-Wert ist "high".

Das Zeichen beziehungsweise die Glyphe wird als Unicode-Zeichen im textlichen Inhalt von `g` erfasst.

### Vokalisation und Übersetzung

Vokalisation, Übersetzung und die zugehörigen Kommentare werden in den Container-Elementen `philology/units/unit` erfasst.

Dabei steht ein `unit` für eine Zeile.

Ein `unit` muss eine `@xml:id` erhalten und kann mit `n` und `@type` spezifiziert werden.

Mittels `@corresp` kann auf Zeilen (`line`) und Teile von Zeilen (`part`) aus der [Transliteration](#transliteration) verwiesen werden, die dieser `unit` entsprechen.
Wenn der entsprechende Abschnitt der Transliteration sich in derselben Datei befindet, wird der Wert der `@xml:id` mit einem vorangestellten "#" als Wert für `@units` angegeben. Wenn sich der entsprechende Abschnitt in der Transliteration nicht in der selben Datei, aber auf der selben Tontafel befindet (also über das gleiche Main-Dokument angesprochen wird), wird `@corresp` eine Kombination aus "main#" und der `@xml:id` des entsprechenden Elements zugewiesen. Wenn der Abschnitt in der Transliteration in einer komplett anderen TextGrid-Ressource erfasst wird, wird dem "#+ID" das Kürzel "TG" + URI der TextGrid-Ressource vorangestellt (Beispielsweise `TG41tvq#line_1.14_I_51_2_overlap`).

Ein `unit` darf `choice`, `transcription`, `translation`, `layer` und `notes` enthalten.

#### HTML-Serialisierung

Eine `unit` wird als eine Zeile dargestellt, die je eine Spalte für Vokalisation und Übersetzung sowie eine Spalte für ein Kommentar-Icon enthält. Kommentare sowie lexikalische und grammatikalische Informationen lassen sich auf Wunsch einblenden.

#### phr

#### w
Einzelne Wörter werden mittels `w` erfasst.

`w` kann eine `@xml:id`, ein `@ana` sowie ein `@lemma` zugewiesen werden. Mittels `@cert` kann angegeben werden, ob man sich sicher ist, dass an dieser Stelle tatsächlich das erfasste Zeichen steht. Wenn ja, wird das `w` mit `@cert="high"` spezifiziert, wenn nicht, dann mit `@cert="low"`. Der Default-Wert ist "high".

Mittels `@corresp` kann auf Segmente von Zeilen (`seg`) aus der [Transliteration](#transliteration) verwiesen werden, die dieser `unit` entsprechen.
Wenn der entsprechende Abschnitt der Transliteration sich in derselben Datei befindet, wird der Wert der `@xml:id` mit einem vorangestellten "#" als Wert für `@units` angegeben. Wenn sich der entsprechende Abschnitt in der Transliteration nicht in der selben Datei, aber auf der selben Tontafel befindet (also über das gleiche Main-Dokument angesprochen wird), wird `@corresp` eine Kombination aus "main#" und der `@xml:id` des entsprechenden Elements zugewiesen. Wenn der Abschnitt in der Transliteration in einer komplett anderen TextGrid-Ressource erfasst wird, wird dem "#+ID" das Kürzel "TG" + URI der TextGrid-Ressource vorangestellt (Beispielsweise `TG41tvr#seg_klv_4hc_pzb`).

`w` darf [`m`](#m), [`lb`](#linebeginning), die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription), [`pc`](#pc) sowie Text enthalten.

#### m

#### Translation
Die Übersetzung wird mittels `translation` erfasst.

Mit dem Attribut `@xml:lang` wird die Sprache der Übersetzung angegeben. Mittels `@cert` kann angegeben werden, ob man sich sicher ist, dass dies die richtige Übersetzung ist. Wenn ja, wird das `translation` mit `@cert="high"` spezifiziert, wenn nicht, dann mit `@cert="low"`. Der Default-Wert ist "high".

`translation` darf [`pc`](#pc), [`phr`](#phr), [`w`](#w), [`lb`](#linebeginning), die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription), [`hi`](#hervorhebungen) sowie Text enthalten.

### Elemente zur Beschreibung der Transkription

#### tei:choice

#### tei:sic

`tei:sic` darf `g`, `pc`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

#### tei:corr

`tei:corr` darf `g`, `pc`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

#### tei:reg

`tei:reg` darf `g`, `pc`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

#### tei:orig

`tei:orig` darf `g`, `pc`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

#### tei:add

`tei:add` darf `g`, `pc`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

##### HTML-Serialisierung
In der HTML-Serialisierung müssen runde Klammern um den Inhalt von `tei:add` eingefügt werden.

Wenn mittels `@prev` auf ein vorhergehendes `tei:add` verwiesen wird, wird keine öffnende runde Klammer vor den Inhalt von `tei:add` gesetzt.

Wenn mittels `@next` auf ein folgendes `tei:add` verwiesen wird, wird keine schließende runde Klammer nach den Inhalt von `tei:add` gesetzt.

#### tei:del
`tei:del` zeigt an, dass an dieser Stelle der Text gelöscht/durchgestrichen worden ist.

Mittels `@next` und `@prev` kann angegeben werden, dass auch (das) folgende/vorhergehende Zeichen gelöscht wurde. Dazu wird auf die `@xml:id` des vorhergehenden/folgenden `tei:del` verwiesen (mit #).

`tei:del` darf `g`, `pc`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

##### HTML-Serialisierung
In der HTML-Serialisierung werden doppelte eckige Klammern um den Inhalt von `tei:del` gesetzt.

Wenn mittels `@prev` auf ein vorhergehendes `tei:del` verwiesen wird, wird keine öffnende doppelte eckige Klammer vor den Inhalt von `tei:del` gesetzt.

Wenn mittels `@next` auf ein folgendes `tei:del` verwiesen wird, wird keine schließende doppelte eckige Klammer nach den Inhalt von `tei:del` gesetzt.

#### tei:unclear
Wenn nicht klar ist, ob ein Zeichen tatsächlich existiert, wird dieses Zeichen in `tei:unclear` erfasst.

Mittels `@next` und `@prev` kann angegeben werden, dass auch (das) folgende/vorhergehende Zeichen nicht sicher ist. Dazu wird auf die `@xml:id` des vorhergehenden/folgenden `tei:unclear` verwiesen (mit #).

`tei:unclear` darf `g`, `pc`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

##### HTML-Serialisierung
In der HTML-Serialisierung müssen runde Klammern um den Inhalt von `tei:unclear` eingefügt werden.

Wenn mittels `@prev` auf ein vorhergehendes `tei:unclear` verwiesen wird, wird keine öffnende runde Klammer vor den Inhalt von `tei:unclear` gesetzt.

Wenn mittels `@next` auf ein folgendes `tei:unclear` verwiesen wird, wird keine schließende runde Klammer nach den Inhalt von `tei:unclear` gesetzt.

#### tei:damage
`tei:damage` zeigt an, dass an dieser Stelle der Text beschädigt ist.

Mittels `@unit` wird spezifiziert, ob ein oder mehrere Zeichen (`character`) oder Zeilen (`line`) beschädigt sind.

Wenn `@unit="line"`, wird mittels `@atLeast` (Mindestanzahl) und `@atMost` (Maximalanzahl) die Anzahl der beschädigten Zeilen angegeben.

Wenn `@unit="character"`, werden die einzelnen Zeichen mittels [`pc@type="non_identifiable_sign_single"` bzw. `pc@type="non_identifiable_sign_multi"`](#Steuerzeichen) erfasst.

Mittels `@next` und `@prev` kann angegeben werden, dass auch (das) folgende/vorhergehende Zeichen beschädigt sind. Dazu wird auf die `@xml:id` des vorhergehenden/folgenden `tei:damage` verwiesen (mit #).

`tei:damage` darf `pc`, `phr`, `w`, `g`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

##### HTML-Serialisierung
In der HTML-Serialisierung werden eckige Klammern um den Inhalt von `tei:damage` gesetzt.

Wenn mittels `@prev` auf ein vorhergehendes `tei:damage` verwiesen wird, wird keine öffnende eckige Klammer vor den Inhalt von `tei:damage` gesetzt.

Wenn mittels `@next` auf ein folgendes `tei:damage` verwiesen wird, wird keine schließende eckige Klammer nach den Inhalt von `tei:damage` gesetzt.

#### tei:restore

`tei:restore` darf `g`, `pc`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

#### tei:supplied
`tei:supplied` zeigt an, dass der Text vom Bearbeiter hinzugefügt wurde.

Mittels `@next` und `@prev` kann angegeben werden, dass auch (das) folgende/vorhergehende Zeichen hinzugefügt wurde. Dazu wird auf die `@xml:id` des vorhergehenden/folgenden `tei:supplied` verwiesen (mit #).

`tei:supplied` darf `g`, `pc`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

##### HTML-Serialisierung
In der HTML-Serialisierung werden spitze Klammern um den Inhalt von `tei:supplied` gesetzt.

Wenn mittels `@prev` auf ein vorhergehendes `tei:supplied` verwiesen wird, wird keine öffnende spitze Klammer vor den Inhalt von `tei:supplied` gesetzt.

Wenn mittels `@next` auf ein folgendes `tei:supplied` verwiesen wird, wird keine schließende spitze Klammer nach den Inhalt von `tei:supplied` gesetzt.

#### tei:surplus
`tei:surplus` zeigt an, dass der Text vom Bearbeiter als überflüssig/redundant eingeschätzt wurde.

Mittels `@next` und `@prev` kann angegeben werden, dass auch (das) folgende/vorhergehende Zeichen überflüssig ist. Dazu wird auf die `@xml:id` des vorhergehenden/folgenden `tei:surplus` verwiesen (mit #).

`tei:surplus` darf `g`, `pc`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

##### HTML-Serialisierung
In der HTML-Serialisierung werden geschweifte Klammern um den Inhalt von `tei:surplus` gesetzt.

Wenn mittels `@prev` auf ein vorhergehendes `tei:surplus` verwiesen wird, wird keine öffnende geschweifte Klammer vor den Inhalt von `tei:surplus` gesetzt.

Wenn mittels `@next` auf ein folgendes `tei:surplus` verwiesen wird, wird keine schließende geschweifte Klammer nach den Inhalt von `tei:surplus` gesetzt.

#### tei:secl

`tei:secl` darf `g`, `pc`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

#### tei:mod

`tei:mod` darf `g`, `pc`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

#### tei:redo

`tei:redo` darf `g`, `pc`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

#### tei:retrace

`tei:retrace` darf `g`, `pc`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

#### tei:undo

`tei:undo` darf `g`, `pc`, `part`, `seg`, `lb`, die [Elemente zur Beschreibung der Transkription](#elemente-zur-beschreibung-der-transkription) und Text enthalten.

### Poetologische Strukturen

#### Kola

Kola werden mit dem Element `kolon` erfasst.

`kolon` kann eine `@xml:id` und ein `@ana` zugewiesen werden.

Mittels `@units` wird angegeben, welcher Abschnitt im philologischen Block (Vokalisation und Übersetzung) diesem Kolon entspricht. Dafür wird der Wert eines `philology//unit/@xml:id` mit einem vorangestellten "#" als Wert für `@units` angegeben.

Ein `kolon` darf `ana`, `motif`, `stylistics` und `notes` enthalten.