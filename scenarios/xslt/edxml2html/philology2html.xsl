<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:edxml="http://sub.uni-goettingen.de/edxml#" xmlns:functx="http://www.functx.com"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs functx tei edxml" version="2.0"
    xpath-default-namespace="http://sub.uni-goettingen.de/edxml#">

    <xsl:include href="libs/html-facsimile.lib.xsl"/>
    <xsl:include href="libs/html-philology.lib.xsl"/>
    <xsl:include href="libs/html-poetological.lib.xsl"/>
    <xsl:include href="libs/html-shared-templates.xsl"/>
    <xsl:include href="libs/html-tei.lib.xsl"/>   

    <xsl:output indent="yes"/>

    <xsl:template match="/">
        <html lang="en-US">
            <head>
                <meta charset="utf-8"/>
                <title>
                    <xsl:apply-templates select="edxml/header/title"/>
                </title>
                <link rel="stylesheet" href="../css/styles.css"/>
                <!-- For files in /scenarios/xslt/edxml2html/samples -->
            </head>
            <body>
                <xsl:apply-templates select="edxml/text"/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="header"/>

    <xsl:template match="text">
        <xsl:apply-templates select="structure[@type = 'poetological']"/>
    </xsl:template>

</xsl:stylesheet>
