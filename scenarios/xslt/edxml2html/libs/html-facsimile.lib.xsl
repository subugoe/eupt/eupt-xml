<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:edxml="http://sub.uni-goettingen.de/edxml#" xmlns:functx="http://www.functx.com"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs functx edxml tei" version="2.0"
    xpath-default-namespace="http://sub.uni-goettingen.de/edxml#">

    <xsl:template match="facsimile">
        <div class="facsimile section"
            id="{if(@xml:id) then (@xml:id) else ('facs_'||generate-id(.))}">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="surface">
        <div class="surface" id="{if(@xml:id) then (@xml:id) else ('surf_'||generate-id(.))}">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="column">
        <xsl:variable name="nr" select="
                if (@n) then
                    (@n)
                else
                    (count(preceding::column) + 1)"/>
        <xsl:variable name="id" select="
                if (@xml:id) then
                    (@xml:id)
                else
                    ('colu_' || generate-id(.))"/>
        <div class="column" id="{$id}" data-n="{$nr}">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="line[@n]">
        <xsl:variable name="nr" select="@n"/>
        <xsl:variable name="auto-nr" select="
                if ($nr) then
                    ($nr)
                else
                    (count(preceding::line) + 1)"/>
        <xsl:variable name="id" select="
                if (@xml:id) then
                    (@xml:id)
                else
                    ('line_' || generate-id(.))"/>
        <xsl:variable name="correspondingNotes" select="edxml:getIDs(., /)"/>
        <div class="line-container">
            <div class="line" id="{$id}">
                <span>
                    <xsl:choose>
                        <xsl:when test="@n">
                            <xsl:attribute name="class">line-nr explicit</xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="class">line-nr calculated</xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:value-of select="$auto-nr"/>
                </span>
                <span class="line-body">
                    <xsl:apply-templates/>
                </span>
                <xsl:if test="$correspondingNotes != ''">
                    <span>
                        <xsl:copy-of select="edxml:generateNoteButton(.)"/>
                    </span>
                </xsl:if>
            </div>
            <xsl:copy-of select="edxml:createNotesContainer($correspondingNotes)"/>
        </div>
    </xsl:template>

    <!-- LINE-PARTS -->
    <xsl:template match="g">
        <xsl:element name="span">
            <xsl:attribute name="class">g</xsl:attribute>
            <xsl:attribute name="id">
                <xsl:value-of select="if(@xml:id) then (@xml:id) else ('g_'||generate-id(.))"/>
            </xsl:attribute>
            <xsl:apply-templates/>
            <xsl:copy-of select="edxml:representCertainty(.)"/>
        </xsl:element>
    </xsl:template>
    

    <xsl:template match="metamark[contains(@rend, 'Line') or contains(@rend, 'line')]">
        <xsl:variable name="rend" select="@rend"/>
        <hr class="metamark-line {$rend}" data-rend="{$rend}"/>
    </xsl:template>

    <xsl:template match="line//part">
        <span class="part" id="{if(@xml:id) then (@xml:id) else ('part_'||generate-id(.))}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="line//seg">
        <xsl:element name="span">
            <xsl:attribute name="class">seg</xsl:attribute>
            <xsl:attribute name="id">
                <xsl:value-of select="if(@xml:id) then (@xml:id) else ('seg_'||generate-id(.))"/>
            </xsl:attribute>
            <xsl:attribute name="onmouseover">
                const corresps = Array.from(document.querySelectorAll('[data-corresp=' + this.id + ']'));
                if(corresps.length > 0){
                    this.classList.add('corresp-hovered');
                    corresps.forEach(corresp => corresp.classList.add('corresp-hovered'));
                }
            </xsl:attribute>
            <xsl:attribute name="onmouseout">
                const corresps = Array.from(document.querySelectorAll('[data-corresp=' + this.id + ']'));
                if(corresps.length > 0){
                    this.classList.remove('corresp-hovered');
                    corresps.forEach(corresp => corresp.classList.remove('corresp-hovered'));
                }
            </xsl:attribute>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <!-- notes -->

    <xsl:template match="facsimile/notes"/>

</xsl:stylesheet>
