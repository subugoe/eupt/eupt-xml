<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:edxml="http://sub.uni-goettingen.de/edxml#" xmlns:functx="http://www.functx.com"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs functx tei edxml" version="2.0"
    xpath-default-namespace="http://sub.uni-goettingen.de/edxml#">

    <!-- Templates for editorial corrections -->

    <xsl:template match="tei:choice">
        <span class="choice">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:sic">
        <span class="sic" id="{if(@xml:id) then (@xml:id) else ('tei_'||generate-id(.))}">
            <span class="tei">(Text:</span>
            <xsl:apply-templates/>
            <span class="tei">)</span>
        </span>
    </xsl:template>

    <xsl:template match="tei:corr">
        <span class="corr" id="{if(@xml:id) then (@xml:id) else ('tei_'||generate-id(.))}">
            <xsl:apply-templates/>
            <span class="tei">
                <span class="super">!</span>
            </span>
        </span>
    </xsl:template>

    <!-- Templates for TEI scribal abbreviations -->

    <xsl:template match="tei:add">
        <span class="add" id="{if(@xml:id) then (@xml:id) else ('tei_'||generate-id(.))}">
            <xsl:if test="not(@prev)">
                <span class="tei">(</span>
            </xsl:if>
            <xsl:apply-templates/>
            <xsl:if test="not(@next)">
                <span class="tei">)</span>
            </xsl:if>
        </span>
    </xsl:template>

    <xsl:template match="tei:damage">
        <span class="damage" id="{if(@xml:id) then (@xml:id) else ('tei_'||generate-id(.))}">
            <xsl:if test="not(@prev)">
                <xsl:choose>
                    <xsl:when test="@degree = 'low'">
                        <span class="tei">⸢</span>
                    </xsl:when>
                    <xsl:when test="@degree = 'medium'">
                        <span class="tei">[</span>
                    </xsl:when>
                    <xsl:when test="not(@degree)">
                        <span class="tei">[</span>
                    </xsl:when>
                </xsl:choose>
            </xsl:if>
            <xsl:apply-templates/>
            <xsl:if test="not(@next)">
                <xsl:choose>
                    <xsl:when test="@degree = 'low'">
                        <span class="tei">⸣</span>
                    </xsl:when>
                    <xsl:when test="@degree = 'medium'">
                        <span class="tei">]</span>
                    </xsl:when>
                    <xsl:when test="not(@degree)">
                        <span class="tei">]</span>
                    </xsl:when>
                </xsl:choose>
            </xsl:if>
        </span>
    </xsl:template>

    <xsl:template match="tei:del">
        <span class="del" id="{if(@xml:id) then (@xml:id) else ('tei_'||generate-id(.))}">
            <xsl:if test="not(@prev)">
                <span class="tei">[[</span>
            </xsl:if>
            <xsl:apply-templates/>
            <xsl:if test="not(@next)">
                <span class="tei">]]</span>
            </xsl:if>
        </span>
    </xsl:template>

    <xsl:template match="tei:supplied">
        <span class="supplied" id="{if(@xml:id) then (@xml:id) else ('tei_'||generate-id(.))}">
            <xsl:if test="not(@prev)">
                <span class="tei">&lt;</span>
            </xsl:if>
            <xsl:apply-templates/>
            <xsl:if test="not(@next)">
                <span class="tei">&gt;</span>
            </xsl:if>
        </span>
    </xsl:template>

    <xsl:template match="tei:surplus">
        <span class="surplus" id="{if(@xml:id) then (@xml:id) else ('tei_'||generate-id(.))}">
            <xsl:if test="not(@prev)">
                <span class="tei">{</span>
            </xsl:if>
            <xsl:apply-templates/>
            <xsl:if test="not(@next)">
                <span class="tei">}</span>
            </xsl:if>
        </span>
    </xsl:template>

    <xsl:template match="tei:unclear">
        <span class="unclear" id="{if(@xml:id) then (@xml:id) else ('tei_'||generate-id(.))}">
            <xsl:if test="not(@prev)">
                <span class="tei">(</span>
            </xsl:if>
            <xsl:apply-templates/>
            <xsl:if test="not(@next)">
                <span class="tei">)</span>
            </xsl:if>
        </span>
    </xsl:template>

    <!-- Default-Template for TEI-elements -->

    <xsl:template match="tei:*">
        <span class="{local-name()}" id="{if(@xml:id) then (@xml:id) else ('tei_'||generate-id(.))}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

</xsl:stylesheet>
