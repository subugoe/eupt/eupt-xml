<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:edxml="http://sub.uni-goettingen.de/edxml#" xmlns:functx="http://www.functx.com"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs functx edxml tei" version="2.0"
    xpath-default-namespace="http://sub.uni-goettingen.de/edxml#">

    <xsl:function name="functx:distinct-deep" as="node()*" xmlns:functx="http://www.functx.com">
        <xsl:param name="nodes" as="node()*"/>

        <xsl:sequence select="
                for $seq in (1 to count($nodes))
                return
                    $nodes[$seq][not(functx:is-node-in-sequence-deep-equal(
                    ., $nodes[position() &lt; $seq]))]
                "/>

    </xsl:function>

    <xsl:function name="functx:is-node-in-sequence-deep-equal" as="xs:boolean"
        xmlns:functx="http://www.functx.com">
        <xsl:param name="node" as="node()?"/>
        <xsl:param name="seq" as="node()*"/>

        <xsl:sequence select="
                some $nodeInSeq in $seq
                    satisfies deep-equal($nodeInSeq, $node)
                "/>

    </xsl:function>

    <xsl:function name="functx:substring-before-if-contains" as="xs:string?"
        xmlns:functx="http://www.functx.com">
        <xsl:param name="arg" as="xs:string?"/>
        <xsl:param name="delim" as="xs:string"/>

        <xsl:sequence select="
                if (contains($arg, $delim))
                then
                    substring-before($arg, $delim)
                else
                    $arg
                "/>

    </xsl:function>

    <xsl:function name="edxml:createNotesContainer">
        <xsl:param name="correspondingNotes" as="node()*"/>
        <xsl:if test="$correspondingNotes != ''">
            <div class="notes-container">
                <ul>
                    <xsl:for-each select="functx:distinct-deep($correspondingNotes)">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </ul>
            </div>
        </xsl:if>
    </xsl:function>

    <xsl:function name="edxml:generateNoteButton">
        <xsl:param name="node" as="node()"/>
        <xsl:variable name="noteIconId" select="generate-id($node)"/>
        <label for="{$noteIconId}" class="notes-icon"/>
        <xsl:element name="input">
            <xsl:attribute name="type">checkbox</xsl:attribute>
            <xsl:attribute name="id">
                <xsl:value-of select="$noteIconId"/>
            </xsl:attribute>
            <xsl:attribute name="onchange" xml:space="preserve">
                if(!this.checked){
                    this.parentElement.parentElement.nextElementSibling.querySelectorAll('input[type="checkbox"]').forEach(elem => {
                        elem.checked = false;
                    });
                }
            </xsl:attribute>
        </xsl:element>
    </xsl:function>

    <xsl:function name="edxml:getCorrespondingNotes">
        <xsl:param name="ID" as="node()*"/>
        <xsl:param name="root" as="node()"/>
        <xsl:variable name="sortOrderTypes" select="'|rek|lx|gr|poet|con|'"/>
        <xsl:for-each select="$root//note[tokenize(replace(@target, '#', ''), ' ') = $ID]">
            <xsl:sort
                select="string-length(substring-before($sortOrderTypes, concat('|', functx:substring-before-if-contains(./@type, ' '), '|')))"
                data-type="number"/>
            <xsl:sort select="count(tokenize(./@type, ' '))" data-type="number"/>
            <xsl:copy-of select="."/>
        </xsl:for-each>
    </xsl:function>

    <xsl:function name="edxml:getIDs">
        <xsl:param name="node" as="node()"/>
        <xsl:param name="root" as="node()"/>
        <xsl:sequence select="
                if ($node//*[@xml:id])
                then
                    (for $child in $node/*
                    return
                        edxml:getIDs($child, $root),
                    edxml:getCorrespondingNotes($node/@xml:id, $root))
                else
                    edxml:getCorrespondingNotes($node/@xml:id, $root)
                "/>
    </xsl:function>

    <xsl:function name="edxml:representCertainty">
        <xsl:param name="node" as="node()"/>
        <xsl:choose>
            <xsl:when test="$node/@cert = 'low'">
                <span class="tei">
                    <span class="super">?</span>
                </span>
            </xsl:when>
        </xsl:choose>
    </xsl:function>


    <!-- Bibliographic notes -->

    <xsl:template match="bibl" mode="container">
        <xsl:param name="bibl-elem"/>
        <xsl:param name="url"/>

        <xsl:variable name="reference" select="document($url)"/>
        <span tabindex="0" class="bib-container">
            <xsl:element name="span">
                <xsl:attribute name="class">bibl-short</xsl:attribute>
                <xsl:attribute name="data-zotero-ref">
                    <xsl:value-of select="@zotero"/>
                </xsl:attribute>
                <xsl:attribute name="onclick" xml:space="preserve">
                    var unclickBibl = function(){
                        document.querySelectorAll(".bibl-click").forEach(elem => elem.classList.remove("bibl-click", "bibl-long"));
                    }
                    unclickBibl();
                    var zoteroId = this.getAttribute("data-zotero-ref");
                    var biblLong = document.querySelector("#bibliography [data-zotero='" + zoteroId + "']");
                    if(biblLong){
                        var event = (window.event || event);
                        biblLong.style.left = Math.max(5, event.pageX - 180).toString() + "px";
                        biblLong.style.top = (event.pageY + 5).toString() + "px";
                        biblLong.classList.add("bibl-click", "bibl-long");
                        event.stopPropagation();
                    }
                    document.querySelectorAll(".content-view").forEach(panel => panel.addEventListener("scroll", unclickBibl, {"once" : true}));
                    document.querySelector("body").addEventListener("click", unclickBibl, {"once" : true});
                </xsl:attribute>
                <xsl:apply-templates select="$bibl-elem/node()"/>
            </xsl:element>
        </span>
    </xsl:template>


    <xsl:template match="bibl">
        <xsl:variable name="zotero_id" select="substring-after(@zotero, 'eupt:')"/>
        <xsl:choose>
            <xsl:when test="$zotero_id != ''">
                <xsl:variable name="url"
                    select="concat('https://eupt-test.sub.uni-goettingen.de/api/eupt/biblio/xml/', $zotero_id)"/>
                <xsl:apply-templates select="." mode="container">
                    <xsl:with-param name="bibl-elem" select="."/>
                    <xsl:with-param name="url" select="$url"/>
                </xsl:apply-templates>
            </xsl:when>
            <xsl:when test="matches(@zotero, '[A-Z]')">
                <xsl:variable name="url"
                    select="concat('https://eupt-test.sub.uni-goettingen.de/api/eupt/biblio/xml/', @zotero)"/>
                <xsl:apply-templates select="." mode="container">
                    <xsl:with-param name="bibl-elem" select="."/>
                    <xsl:with-param name="url" select="$url"/>
                </xsl:apply-templates>
            </xsl:when>
            <xsl:otherwise>
                <span tabindex="0" class="bib-container">
                    <span class="bibl-short">
                        <xsl:apply-templates/>
                    </span>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="div" mode="bibl-long">
        <span class="bibl-long">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="span">
        <span class="{@class}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <!--  Highlightings  -->

    <xsl:template match="hi[@rend = 'italics'] | hi[not(@rend)]">
        <span class="hi italic" id="{if(@xml:id) then (@xml:id) else ('hi_'||generate-id(.))}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="hi[@rend = 'bold']">
        <span class="hi bold" id="{if(@xml:id) then (@xml:id) else ('hi_'||generate-id(.))}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="hi[@rend = 'small_caps']">
        <span class="hi small-caps" id="{if(@xml:id) then (@xml:id) else ('hi_'||generate-id(.))}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="hi[@rend = 'sub']">
        <sub class="hi" id="{if(@xml:id) then (@xml:id) else ('hi_'||generate-id(.))}">
            <xsl:apply-templates/>
        </sub>
    </xsl:template>

    <xsl:template match="hi[@rend = 'super']">
        <sup class="hi" id="{if(@xml:id) then (@xml:id) else ('hi_'||generate-id(.))}">
            <xsl:apply-templates/>
        </sup>
    </xsl:template>

    <xsl:template match="hi[@rend = 'under']">
        <span class="hi underline" id="{if(@xml:id) then (@xml:id) else ('hi_'||generate-id(.))}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <!-- Line Beginnings -->

    <xsl:template match="lb[@n]">
        <span class="lb item">
            <xsl:value-of select="data(@n)"/>
        </span>
    </xsl:template>

    <!--  Notes  -->

    <xsl:template match="note">
        <li class="note-item">
            <xsl:variable name="noteItemId" select="generate-id()"/>
            <xsl:variable name="types" select="replace(replace(@type, 'con', 'inh'), ' ', ' / ')"/>
            <button>
                <label for="{$noteItemId}">
                    <span class="noteType">
                        <xsl:value-of select="$types"/>
                    </span>
                    <xsl:apply-templates select="label"/>
                </label>
            </button>
            <input id="{$noteItemId}" type="checkbox"/>
            <span>
                <xsl:apply-templates select="node()[not(self::label)]"/>
            </span>
        </li>
    </xsl:template>

    <!--  Paragraphs  -->

    <xsl:template match="p">
        <p id="{if(@xml:id) then (@xml:id) else ('p_'||generate-id(.))}">
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <!-- Quotation Marks -->

    <xsl:template match="quote">
        <xsl:choose>
            <xsl:when test="@rend = 'en'">
                <xsl:choose>
                    <xsl:when test=".[ancestor::quote]">
                        <xsl:text>‘</xsl:text>
                        <xsl:apply-templates/>
                        <xsl:text>’</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>“</xsl:text>
                        <xsl:apply-templates/>
                        <xsl:text>”</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="@rend = 'fr'">
                <xsl:choose>
                    <xsl:when test=".[ancestor::quote]">
                        <xsl:text>‹</xsl:text>
                        <xsl:apply-templates/>
                        <xsl:text>›</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>«</xsl:text>
                        <xsl:apply-templates/>
                        <xsl:text>»</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test=".[ancestor::quote]">
                        <xsl:text>‚</xsl:text>
                        <xsl:apply-templates/>
                        <xsl:text>‘</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>„</xsl:text>
                        <xsl:apply-templates/>
                        <xsl:text>“</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- References -->

    <xsl:template match="ref">
        <a href="{@target}" target="_blank">
            <xsl:apply-templates/>
        </a>
    </xsl:template>

    <!-- Scribal abbreviations -->

    <xsl:template match="pc">
        <span class="scribal" id="{if(@xml:id) then (@xml:id) else ('g_'||generate-id(.))}">
            <xsl:choose>
                <xsl:when test="@type = 'non_identifiable_sign_multi'">
                    <xsl:value-of select="'&#8230;'"/>
                </xsl:when>
                <xsl:when test="@type = 'non_identifiable_sign_single'">
                    <xsl:value-of select="'x'"/>
                </xsl:when>
                <xsl:when test="@type = 'word_boundary_separator'">
                    <xsl:value-of select="'&#8201;&#46;'"/>
                    <xsl:copy-of select="edxml:representCertainty(.)"/>
                    <xsl:value-of select="'&#8201;'"/>
                </xsl:when>
            </xsl:choose>
            <xsl:if test="not(@type = 'word_boundary_separator')">
                <xsl:copy-of select="edxml:representCertainty(.)"/>
            </xsl:if>
        </span>
    </xsl:template>

    <!-- TextBlock -->

    <xsl:template match="textBlock">
        <span class="textBlock">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

</xsl:stylesheet>
