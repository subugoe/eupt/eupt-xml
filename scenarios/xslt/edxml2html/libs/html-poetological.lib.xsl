<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs tei" version="2.0"
    xpath-default-namespace="http://sub.uni-goettingen.de/edxml#">
    
    <xsl:template match="structure//ana"/>
    
    <xsl:template match="kolon">
        <xsl:variable name="philologicalBlock" select="//philology"/>
        <xsl:for-each select="tokenize(@units)">
            <xsl:variable name="unitID" select="substring-after(., '#')"/>
            <xsl:apply-templates select="$philologicalBlock//unit[@xml:id=$unitID]"/>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="structure//notes"/>
    
    <xsl:template match="stanza">
        <div class="stanza"
            id="{if(@xml:id) then (@xml:id) else ('stanza_'||generate-id(.))}">
            <fieldset>
                <legend>Strophe</legend>
                <xsl:apply-templates/>
            </fieldset>
        </div>
    </xsl:template>
    
    <xsl:template match="structure[@type='poetological']">
        <div class="philology section"
            id="{if(@xml:id) then (@xml:id) else ('poet_'||generate-id(.))}">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="structure//units">        
        <xsl:apply-templates/>           
    </xsl:template>
    
    <xsl:template match="verse">
        <div class="verse"
            id="{if(@xml:id) then (@xml:id) else ('verse_'||generate-id(.))}">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
</xsl:stylesheet>