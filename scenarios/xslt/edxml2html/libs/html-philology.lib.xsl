<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:edxml="http://sub.uni-goettingen.de/edxml#" xmlns:functx="http://www.functx.com"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs functx edxml tei" version="2.0"
    xpath-default-namespace="http://sub.uni-goettingen.de/edxml#">

    <xsl:function name="edxml:correspValues">
        <xsl:param name="node" as="node()"/>
        <xsl:value-of select="
                if ($node/@corresp) then
                    (for $token in tokenize($node/@corresp, ' ')
                    return
                        if (starts-with($token, '#')) then
                            substring-after($token, '#')
                        else
                            if (starts-with($token, 'main#')) then
                                substring-after($token, 'main#')
                            else
                                (if (starts-with($token, 'TG#')) then
                                    ()
                                else
                                    ('no_valid_corresp_data')))
                else
                    (false())"/>
    </xsl:function>

    <xsl:function name="edxml:itemElements">
        <xsl:param name="node" as="node()*"/>
        <xsl:for-each select="$node">
            <xsl:choose>
                <xsl:when test="self::text()[not(normalize-space(.) = '')]">
                    <span class="text item">
                        <xsl:value-of select="."/>
                    </span>
                </xsl:when>
                <xsl:when test="self::text()[not(. = '')]">
                    <xsl:value-of select="."/>
                </xsl:when>
                <xsl:when test="self::w or self::phr or self::lb">
                    <xsl:apply-templates select="."/>
                </xsl:when>
                <xsl:when test="self::element()">
                    <span class="item">
                        <xsl:apply-templates select="."/>
                    </span>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
    </xsl:function>

    <xsl:template match="@xml:lang">
        <xsl:attribute name="data-lang">
            <xsl:value-of select="data(.)"/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="@n">
        <xsl:attribute name="data-n">
            <xsl:value-of select="data(.)"/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="philology//unit">
        <xsl:variable name="id" select="
                if (@xml:id) then
                    (@xml:id)
                else
                    ('facs_' || generate-id(.))"/>
        <xsl:variable name="corresp" select="edxml:correspValues(.)"/>
        <xsl:variable name="correspondingNotes" select="edxml:getIDs(.//transcription, /)"/>
        <div class="unit" id="{$id}" data-corresp="{$corresp}">
            <div class="unit-container">
                <xsl:apply-templates/>
                <div class="notes section-tab">
                    <xsl:if test="$correspondingNotes != ''">
                        <xsl:copy-of select="edxml:generateNoteButton(.)"/>
                    </xsl:if>
                </div>
            </div>
            <xsl:copy-of select="edxml:createNotesContainer($correspondingNotes)"/>
        </div>
    </xsl:template>

    <xsl:template match="philology//transcription">
        <div class="transcription section-tab">
            <div class="vocalisation-text">
                <xsl:apply-templates select="@*"/>
                <xsl:copy-of select="edxml:itemElements(node())"/>                
            </div>
            <div class="annotations">
                <xsl:variable name="concattedIDs">
                    <xsl:value-of
                        select="concat(string-join(.//w/@xml:id, '_annotation'), '_annotation')"/>
                </xsl:variable>
                <xsl:for-each select=".//w[@ana or @lemma]">
                    <input type="radio" name="{$concattedIDs}" id="{@xml:id||'_annotation'}"/>
                    <span class="annotation">
                        <span class="ana">
                            <xsl:choose>
                                <xsl:when test="@ana">
                                    <xsl:value-of select="@ana"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text> </xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </span>
                        <span class="lemma">
                            <xsl:choose>
                                <xsl:when test="@lemma">
                                    <xsl:value-of select="@lemma"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text> </xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </span>
                    </span>
                </xsl:for-each>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="transcription/@type">
        <xsl:attribute name="data-type">
            <xsl:value-of select="data(.)"/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="philology//translation">
        <xsl:element name="div">
            <xsl:attribute name="class">translation section-tab</xsl:attribute>
            <xsl:apply-templates select="@*[name() != 'cert']"/>
            <xsl:copy-of select="edxml:itemElements(node())"/>
            <xsl:copy-of select="edxml:representCertainty(.)"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="phr">
        <xsl:variable name="id" select="
                if (@xml:id) then
                    (@xml:id)
                else
                    ('facs_' || generate-id(.))"/>
        <xsl:variable name="corresp" select="edxml:correspValues(.)"/>
        <span class="phr" id="{$id}" data-corresp="{$corresp}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>


    <xsl:template match="w">
        <xsl:variable name="corresp" select="edxml:correspValues(.)"/>
        <xsl:element name="span">
            <xsl:attribute name="class">word_container</xsl:attribute>
            <xsl:element name="span">
                <xsl:attribute name="class">w item</xsl:attribute>
                <xsl:attribute name="id">
                    <xsl:value-of select="@xml:id"/>
                </xsl:attribute>
                <xsl:attribute name="tabindex">1</xsl:attribute>
                <xsl:attribute name="data-corresp">
                    <xsl:value-of select="$corresp"/>
                </xsl:attribute>
                <xsl:attribute name="onmouseover">
                    const corresp = document.getElementById(this.getAttribute('data-corresp'));
                    if(corresp){
                        this.classList.add('corresp-hovered');
                        corresp.classList.add('corresp-hovered');
                    }
                </xsl:attribute>
                <xsl:attribute name="onmouseout">
                    const corresp = document.getElementById(this.getAttribute('data-corresp'));
                    if(corresp){
                        this.classList.remove('corresp-hovered');
                        corresp.classList.remove('corresp-hovered');
                    }
                </xsl:attribute>
                <label class="body" for="{@xml:id||'_annotation'}">
                    <xsl:apply-templates/>
                </label>
                <xsl:copy-of select="edxml:representCertainty(.)"/>
            </xsl:element>
            <xsl:element name="span">
                <xsl:attribute name="class">ling_information</xsl:attribute>
                <xsl:element name="span">
                    <xsl:attribute name="class">morph</xsl:attribute>
                    <xsl:value-of select="@ana"/>
                </xsl:element>
                <xsl:element name="span">
                    <xsl:attribute name="class">lem</xsl:attribute>
                    <xsl:value-of select="@lemma"/>
                </xsl:element>
            </xsl:element>        
        </xsl:element>
    </xsl:template>

    <xsl:template match="philology//notes"/>

</xsl:stylesheet>
