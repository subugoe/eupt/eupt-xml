<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" 
    queryBinding="xslt2"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:sqf="http://www.schematron-quickfix.com/validator/process"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
    
    <sch:ns prefix="edxml" uri="http://sub.uni-goettingen.de/edxml#"/>
    <sch:ns prefix="rdf" uri="http://www.w3.org/1999/02/22-rdf-syntax-ns#"/>
    <sch:ns prefix="skos" uri="http://www.w3.org/2004/02/skos/core#"/>
    <sch:ns prefix="tei" uri="http://www.tei-c.org/ns/1.0"/>
    
    <sch:let name="fname" value="tokenize(root()/base-uri(), '/')[last()]"/>
    <sch:let name="rel-dir" value="substring-before(root()/base-uri(), $fname)"/>
    
    <sch:let name="dictionaries">
        <xsl:for-each select="//edxml:externalResources/edxml:dictionary">
            <lemmalist prefix="{./@prefix}" rdir="{$rel-dir}" href="{./@href}">
                <prefix><xsl:value-of select="./@prefix"/></prefix>
                <href><xsl:value-of select="./@href"/></href>
                <xsl:choose>
                    <xsl:when test="doc-available(./@href)">
                        <xsl:copy-of select="doc(./@href)"/>
                    </xsl:when>
                    <xsl:when test="doc-available($rel-dir || ./@href)">
                        <xsl:copy-of select="doc($rel-dir || ./@href)"/>
                    </xsl:when>
                    <xsl:otherwise/>
                </xsl:choose>
            </lemmalist>
        </xsl:for-each>
    </sch:let>
    
    <sch:let name="vocabularies">
        <xsl:for-each select="//edxml:externalResources/edxml:vocabulary">
            <terms prefix="{./@prefix}" rdir="{$rel-dir}" href="{./@href}">
                <prefix><xsl:value-of select="./@prefix"/></prefix>
                <href><xsl:value-of select="./@href"/></href>
                <xsl:choose>
                    <xsl:when test="doc-available(./@href)">
                        <xsl:copy-of select="doc(./@href)"/>
                    </xsl:when>
                    <xsl:when test="doc-available($rel-dir || ./@href)">
                        <xsl:copy-of select="doc($rel-dir || ./@href)"/>
                    </xsl:when>
                    <xsl:otherwise/>
                </xsl:choose>
            </terms>
        </xsl:for-each>
    </sch:let>
    
    <sch:pattern>
        <sch:rule context="@lemma[count(tokenize(., ':')) = 2]">
            <sch:let name="prefix" value="tokenize(., ':')[1]"></sch:let>
            <sch:let name="lemmacat" value="$dictionaries/lemmalist[@prefix = $prefix]"/>
            <sch:let name="value" value="tokenize(., ':')[2]"/>
            
            <sch:assert test="$lemmacat" role="error">Die Lemmacat-Datei mit dem Präfix "<sch:value-of select="$prefix"/>" konnte nicht gefunden werden!</sch:assert>
            
            <sch:report test="$lemmacat and not($value = $lemmacat//(@xml:id, @lid))" role="error">Das Lemma "<sch:value-of select="$value"/>" konnte nicht in "<sch:value-of select="$prefix"/>" gefunden werden!</sch:report>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="edxml:term/@key[count(tokenize(., ':')) = 2]">
            <sch:let name="prefix" value="tokenize(., ':')[1]"></sch:let>
            <sch:let name="vocab" value="$vocabularies/terms[@prefix = $prefix]"/>
            <sch:let name="terms" value="for $term in $vocab//skos:Concept/@rdf:about return tokenize($term, '/')[last()]"/>
            <sch:let name="value" value="tokenize(., ':')[2]"/>
            
            <sch:assert test="$vocab" role="error">Die Vokabular-Datei mit dem Präfix "<sch:value-of select="$prefix"/>" konnte nicht gefunden werden!</sch:assert>
            
            <sch:report test="$vocab and not($value = $terms)" role="error">Der Term "<sch:value-of select="$value"/>" konnte nicht in "<sch:value-of select="$prefix"/>" gefunden werden! Erlaubte Terme sind: <sch:value-of select="for $term in $terms return $prefix||':'||$term||' '"/></sch:report>
        </sch:rule>
    </sch:pattern>
    
</sch:schema>